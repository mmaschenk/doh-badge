#define S2 12 // Define pins for color sensor
#define S3 10
#define sensorOut 13
int rawRed = 0, red = 0 , rawGreen = 0, green = 0, rawBlue = 0, blue = 0; // Define color variables used
float brightness = 0.2; // Maximum brightness where 0 = 0% and 1 = 100%

void setup() {
  // put your setup code here, to run once:
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  Serial.begin(115200); // Initialize serial Port
  pinMode(sensorOut, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:

  
  // Setting Red filtered photodiodes to be read
  digitalWrite(S2, LOW); // Configure which color the diode reads
  digitalWrite(S3, LOW);
  rawRed = pulseIn(sensorOut, LOW);   // Measure amount of red light
  red = map(rawRed, 45, 600, 255, 0); // Calibrate and scale result to RGB values
  red = constrain(red, 0, 255);     // Make sure values do not exceed RGB values
  Serial.printf("R: %d \t", red);  // Print color value to serial
  delay(5);
  

  // Setting Green filtered photodiodes to be read
  digitalWrite(S2, HIGH);
  digitalWrite(S3, HIGH);
  rawGreen = pulseIn(sensorOut, LOW);
  green = map(rawGreen, 50, 700, 255, 0);
  green = constrain(green, 0, 255);
  Serial.printf("G: %d \t", green);
  delay(5);


  // Setting Blue filtered photodiodes to be read
  digitalWrite(S2, LOW);
  digitalWrite(S3, HIGH);
  rawBlue = pulseIn(sensorOut, LOW);
  blue = map(rawBlue, 40, 600, 255, 0);
  blue = constrain(blue, 0, 255);
  Serial.printf("B: %d \n", blue);
  delay(100);
}

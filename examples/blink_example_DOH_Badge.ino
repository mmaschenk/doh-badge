#include <Adafruit_DotStar.h> // Include libraries
#include <SPI.h>

#define NUMPIXELS 7 // Number of LEDs in RGB strip
#define DATAPIN 14  // Define pins for SPI communication to the LEDs
#define CLOCKPIN 16 // Define pins for SPI communication to the LEDs
Adafruit_DotStar strip(NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BGR);  // Create LED Strip

void setup() {
  strip.begin(); // Initialize pins for LED output
  strip.show();  // Update LEDs

}

void loop() {
  //The text in gray is a comment, if you want to control one of the leds we can use the following command:
  //strip.setPixelColor(LED number, red value(0-255), green value(0-255), blue value(0-255));
  
  strip.setPixelColor(0,255,255,255); // this sets the first    led to white
  strip.setPixelColor(1,255,0,0);     // this sets the second   led to red
  strip.setPixelColor(2,0,255,0);     // this sets the third    led to green
  strip.setPixelColor(3,0,0,255);     // this sets the fourth   led to blue
  strip.setPixelColor(4,255,255,0);   // this sets the fifth    led to magenta
  strip.setPixelColor(5,0,255,255);   // this sets the sixth    led to cyan
  strip.setPixelColor(6,255,0,255);   // this sets the seventh  led to yellow
  strip.show();                       // Refresh RGB strip
  delay(5000);                        // this will pause the loop for 5 seconds



  //if you want to change the lights of all leds you can make a for loop
  
  for ( int i = 0; i < NUMPIXELS; i++) {
    strip.setPixelColor(i, 0, 0, 0);   // turn of all leds
  }
  strip.show();                       // Refresh RGB strip
  delay(1000);                        // this will pause the loop for 1 seconds
  // note how the "strip show" and "delay" are outside the for-loop, this makes it appear that all the lights turn of at once

  
  for ( int i = 0; i < NUMPIXELS; i++) {
    strip.setPixelColor(i, 0, 0, 255);  // turn on the leds blue
    strip.show();                       // Refresh RGB strip
    delay(500);
  }
  // note how the "strip show" and "delay" are inside the for-loop, this makes it appear that the lights turn on one by one
  
}
